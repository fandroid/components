package com.frumle.components.example

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.frumle.components.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

}