package com.frumle.components.utils

interface Logger {
    fun logd(tag: String, msg: String)
    fun logw(tag: String, msg: String)
    fun loge(tag: String, msg: String)
}