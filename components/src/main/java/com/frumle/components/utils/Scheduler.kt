package com.frumle.components.utils

interface Scheduler {
    fun executeDelayed(time: Long, task: Runnable)
}