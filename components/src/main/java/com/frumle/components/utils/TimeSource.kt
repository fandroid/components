package com.frumle.components.utils

interface TimeSource {
    fun currentTime(): Long
}