package com.frumle.components

import com.frumle.components.core.ComponentManager
import com.frumle.components.core.ComponentRetention
import com.frumle.components.core.Container
import com.frumle.components.core.DependencyResolutionException
import com.frumle.components.core.acquireOrCreateNewContainer

interface ComponentOwner: ComponentNode {
    val componentOwnerConfiguration: ComponentManager.Configuration
    val componentManager: ComponentManager
}

inline fun <reified D> ComponentNode.findDependency(): Pair<D, Container<*>>? {
    var node = parentNode
    while(node != null){
        node.container?.let {
            if(it.component is D){
                return Pair(it.component, it)
            }
        }
        node = node.parentNode
    }
    return null
}

fun <D, C> ComponentOwner.configure(
    componentClass: Class<C>,
    ownerClass: Class<*>,
    key: Lazy<String>?,
    retention: ComponentRetention,
    componentBuilderProvider: ()->ComponentBuilder<D, C>,
    dependencyProvider: ()->Pair<D, Container<*>>
): Lazy<C> {
    val containerProvider = {
        // Obtain the dependency and host for the component
        val (dependency, host) = dependencyProvider()

        // Acquire the component's container or create a new one if not found
       host.acquireOrCreateNewContainer(componentClass){
            componentBuilderProvider().dependency(dependency).build()
        }
    }
    componentOwnerConfiguration.apply {
        this.ownerClass = ownerClass
        this.ownerKey = key
        this.retention = retention
        this.containerProvider = containerProvider
    }

    @Suppress("UNCHECKED_CAST")
    return lazy(LazyThreadSafetyMode.NONE) { componentManager.container.component as C }
}


inline fun <reified D, reified C> ComponentOwner.components(
    key: Lazy<String>? = null,
    retention: ComponentRetention = ComponentRetention.None,
    noinline componentBuilderProvider: () -> ComponentBuilder<D, C>
): Lazy<C> {
    return configure(C::class.java, this.javaClass, key, retention, componentBuilderProvider) {
        findDependency() ?: throw DependencyResolutionException(C::class.java, this@components.javaClass)
    }
}


inline fun <reified T: Any> ComponentOwner.store(crossinline initializer: ()->T): Lazy<T> =
    lazy(LazyThreadSafetyMode.NONE) {
        with(componentManager.store){
            val key = T::class.java
            get(key) ?: initializer.invoke().also { put(key, it) }
        }
    }

inline fun <reified T: Any> ComponentOwner.storeContextual(crossinline initializer: ()->T): Lazy<T> =
    lazy(LazyThreadSafetyMode.NONE) {
        with(componentManager.store){
            val key = T::class.java
            getContextual(key) ?: initializer.invoke().also { putContextual(key, it) }
        }
    }

inline fun <reified T: Any> ComponentOwner.sharedStore(crossinline initializer: ()->T): Lazy<T> =
    lazy(LazyThreadSafetyMode.NONE) {
        with(componentManager.sharedStore){
            val key = T::class.java
            get(key) ?: initializer.invoke().also { put(key, it) }
        }
    }

inline fun <reified T: Any> ComponentOwner.sharedStoreContextual(crossinline initializer: ()->T): Lazy<T> =
    lazy(LazyThreadSafetyMode.NONE) {
        with(componentManager.sharedStore){
            val key = T::class.java
            getContextual(key) ?: initializer.invoke().also { putContextual(key, it) }
        }
    }