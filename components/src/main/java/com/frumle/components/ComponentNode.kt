package com.frumle.components

import com.frumle.components.core.Container

/**
 *  ComponentNode is just the interface providing the access to the tree of Containers.
 *
 *  This is the base interface of the client side of the library, which helps the
 *  user to find the dependency D and the host for their component C.
 *
 *  The implementation shouldn't inherit this interface directly,
 *  instead, use ComponentOwner for particular node implementation.
 *
 */
interface ComponentNode {
    val container: Container<*>? get() = null
    val parentNode: ComponentNode?
}
