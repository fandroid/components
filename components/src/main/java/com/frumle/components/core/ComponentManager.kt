package com.frumle.components.core

class ComponentManager {

    companion object {
        const val TAG = "Components::CompManager"
    }

    class Configuration {

        internal var ownerClass: Class<*>? = null
        internal var ownerKey: Lazy<String>? = null
        internal var retention: ComponentRetention = ComponentRetention.None
        internal var containerProvider: (() -> ContainerHandle<*>)? = null

        fun set(configuration: Configuration){
            ownerClass = configuration.ownerClass
            ownerKey = configuration.ownerKey
            retention = configuration.retention
            containerProvider = configuration.containerProvider
        }

    }

    // Private
    private val storeKey = lazy(LazyThreadSafetyMode.NONE) {
        val ownerName = configuration.ownerClass?.let {
            it.canonicalName
                ?: throw IllegalStateException("Component owner must have a canonical class name.")
        } ?: throw EarlyComponentAccessException()
        configuration.ownerKey?.let { "$ownerName:${it.value}" } ?: ownerName
    }
    private var _containerHandle: ContainerHandle<*>? = null

    val configuration = Configuration()

    internal val container: Container<*>
        get() = _containerHandle?.container ?: throw EarlyComponentAccessException(configuration.ownerClass)

    val store: Store
        get() = _containerHandle?.store ?: throw EarlyComponentAccessException(configuration.ownerClass)

    val sharedStore: Store
        get() = _containerHandle?.sharedStore ?: throw EarlyComponentAccessException(configuration.ownerClass)

    // Public interface
    fun acquireContainer(){
        if(_containerHandle == null){
            val handle = configuration.containerProvider?.invoke()
                ?: throw ComponentAccessException("Container owner hasn't been initialized for: ${configuration.ownerClass}.")

            _containerHandle = handle.apply {
                acquireStores(storeKey.value)
                store.retention = configuration.retention
            }
        }
    }

    fun releaseContainer(){
        _containerHandle?.let {
            _containerHandle = null
            it.release()
        }
    }

    fun clearContextualData(){
        container.clearContextualData()
    }

}
