package com.frumle.components.core

import android.os.Handler
import android.os.Looper
import com.frumle.components.android.utils.AndroidLogcatLogger
import com.frumle.components.android.utils.AndroidScheduler
import com.frumle.components.android.utils.AndroidTimeSource
import com.frumle.components.utils.Logger
import com.frumle.components.utils.Scheduler
import com.frumle.components.utils.TimeSource

class ComponentsConfiguration(
    val timeSource: TimeSource,
    val scheduler: Scheduler,
    val logger: Logger
)

class ComponentConfigurationBuilder {

    var timeSource: TimeSource? = null
    var scheduler: Scheduler? = null
    var logger: Logger? = null

    fun build(): ComponentsConfiguration {
        return ComponentsConfiguration(
            timeSource ?: defaultTimeSource,
            scheduler ?: defaultScheduler,
            logger ?: defaultLogger
        )
    }

    private val defaultTimeSource get() = AndroidTimeSource()

    private val defaultScheduler get() = AndroidScheduler(Handler(Looper.getMainLooper()))

    private val defaultLogger get() = AndroidLogcatLogger("Components")

}