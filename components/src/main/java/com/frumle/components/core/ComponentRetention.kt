package com.frumle.components.core

import java.util.concurrent.TimeUnit


sealed class ComponentRetention {

    object None: ComponentRetention(){
        override fun compareTo(retention: ComponentRetention): Int {
            return -1
        }
    }

    object Forever: ComponentRetention(){
        override fun compareTo(retention: ComponentRetention): Int {
            return 1
        }
    }

    class Temporary(val retentionTime: Long): ComponentRetention(){
        override fun compareTo(retention: ComponentRetention): Int {
            return when(retention){
                None -> { 1 }
                Forever -> { -1 }
                is Temporary -> {
                    if(retentionTime > retention.retentionTime){
                        1
                    } else {
                        -1
                    }
                }
            }
        }
    }

    abstract fun compareTo(retention: ComponentRetention): Int

}

fun temporary(duration: Long, units: TimeUnit): ComponentRetention {
    return ComponentRetention.Temporary(units.toMillis(duration))
}