package com.frumle.components.core

class ContainerHandle<C>(
    val container: Container<C>
) {

    lateinit var store: Store
    lateinit var sharedStore: Store

    init {
        container.acquire()
    }

    fun acquireStores(key: String){
        sharedStore = container.acquireSharedStore()
        store = container.acquireStore(key)
    }

    var retention: ComponentRetention
        get() = store.retention
        set(value) { store.retention = value }

    fun release(){
        container.releaseSharedStore()
        container.releaseStore(store)
        container.release()
    }

}