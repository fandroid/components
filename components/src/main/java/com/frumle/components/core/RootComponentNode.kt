package com.frumle.components.core

import com.frumle.components.ComponentNode

open class RootComponentNode(
    override val container: Container<*>
): ComponentNode {

    override val parentNode: ComponentNode? = null

}

inline fun <reified C> rootNode(
    component: C,
    configuration: ComponentConfigurationBuilder.()->Unit
): ComponentNode {
    val config = ComponentConfigurationBuilder().apply(configuration).build()
    val container = Container(C::class.java, component, null, config)
    return RootComponentNode(container)
}
