package com.frumle.components.core

class Store {

    companion object {
        const val TAG = "Components::Store"
    }

    internal var storeKey: String? = null

    internal var retention: ComponentRetention = ComponentRetention.None
        set(value) {
            // allow retention change only when exclusively acquired
            assert(useCounter == 1){
                "Failed to change retention for the store $storeKey. Use counter: $useCounter."
            }
            if(field != value){
                field = value
            }
        }

    internal var accessTimestamp: Long = 0L
    internal var useCounter: Int = 0

    private val values = mutableMapOf<Class<*>, Any>()
    private val contextualValues = mutableMapOf<Class<*>, Any>()


    fun <T: Any> put(key: Class<T>, instance: T) {
        values[key] = instance
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: Any> get(key: Class<T>): T? {
        return values[key] as? T
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: Any> remove(key: Class<T>): T? {
        return values.remove(key) as? T
    }


    fun <T: Any> putContextual(key: Class<T>, instance: T) {
        contextualValues[key] = instance
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: Any> getContextual(key: Class<T>): T? {
        return contextualValues[key] as? T
    }

    @Suppress("UNCHECKED_CAST")
    fun <T: Any> removeContextual(key: Class<T>): T? {
        return contextualValues.remove(key) as? T
    }


    fun clearContextual(){
        clear(contextualValues)
    }

    fun destroy(){
        clear(contextualValues)
        clear(values)
    }


    private fun clear(values: MutableMap<Class<*>, Any>){
        values.forEach { entry ->
            entry.value.let {
                if(it is AutoCloseable) it.close()
            }
        }
        values.clear()
    }

}