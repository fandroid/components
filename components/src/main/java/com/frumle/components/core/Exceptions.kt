package com.frumle.components.core

class ComponentOwnerConfigurationException(msg: String): Exception(msg)

class ComponentAccessException(msg: String): Exception(msg)

class DependencyResolutionException(componentClass: Class<*>, componentOwnerClass: Class<*>):
    Exception("Failed to resolve dependencies for: $componentClass. Component owner: $componentOwnerClass.")

class EarlyComponentAccessException(ownerClass: Class<*>? = null):
    Exception("Container for the component ${ownerClass?.let { "(owner: $it)" } ?: ""} has not been acquired yet.")