package com.frumle.components.core

const val STORE_EXPIRATION_DELAY = 500

/**
 *
 *  Container for an initialized component.
 *
 *  Each container can have zero or more children containers,
 *  forming a tree of containers.
 *
 */
class Container<C> (
    private val componentClass: Class<C>,
    val component: C,
    private val parent: Container<*>?,
    internal val configuration: ComponentsConfiguration
) {

    companion object {
        const val TAG = "Container"
    }

    private var _children: MutableMap<Class<*>, Container<*>>? = null
    private var _stores = mutableMapOf<String, Store>()
    private val _sharedStore = Store()
    private var _useCounter = 0
    private var _isDestroyed = false

    init {
        configuration.logger.logd(TAG, "CREATED for component ${componentClass.simpleName}.")
    }


    @Suppress("UNCHECKED_CAST")
    internal fun <T> getChildContainer(componentClass: Class<T>): ContainerHandle<T>? {
        return _children?.get(componentClass)?.let {
            ContainerHandle(it as Container<T>)
        }
    }

    internal fun <T> addChildContainer(componentClass: Class<T>, component: T): ContainerHandle<T> {
        val container = Container(componentClass, component, this, configuration)
        requireChildren().let {
            assert(!it.containsKey(componentClass))
            it[componentClass] = container
        }
        return ContainerHandle(container)
    }

    private fun removeChildContainer(container: Container<*>){
        val removed = _children?.remove(container.componentClass)
        assert(removed === container)

        if(requiresDestruction)
            destroy()
    }

    internal fun acquire(){
        _useCounter++
//        Log.d(TAG, "acquire container $this, ${_stores.keys}, $_useCounter")
    }

    internal fun release(){
        _useCounter--
//        Log.d(TAG, "release container $this, ${_stores.keys}, $_useCounter")
        if(requiresDestruction)
            destroy()
    }

    internal fun clearContextualData(){
        configuration.logger.logd(TAG, "CLEAR CONTEXTUAL for ${componentClass.simpleName}.")
        _children?.forEach { it.value.clearContextualData() }
        _stores.forEach { it.value.clearContextual() }
        _sharedStore.clearContextual()
    }


    /**
     *  Client shared store.
     *
     * This instance of the store is shared between all the clients of the component.
     *  I.e. every client (ComponentOwner) who has an access to the container,
     *  can acquire the shared store.
     *
     *  While the regular stores will be destroyed at some point when the
     *  client (component owner) is destroyed, the shared store
     *  will be kept alive for the whole lifetime of the component's container.
     *
     */
    internal fun acquireSharedStore(): Store {
        return _sharedStore.also { it.useCounter++ }
    }

    internal fun releaseSharedStore(){
        _sharedStore.useCounter--
    }


    /**
     *  Client stores. Each client have its own store in the component's container.
     *  Client can acquire its store using the key.
     *
     */
    internal fun acquireStore(key: String): Store {
        return _stores.getOrPut(key) {
            Store().also { it.storeKey = key }
        }.also { it.useCounter++ }
    }

    internal fun releaseStore(store: Store) {
        val found = _stores[store.storeKey]
        assert(found === store)

        if(--store.useCounter == 0){
            when(val retention = store.retention){
                ComponentRetention.Forever -> { /* NOOP */ }
                ComponentRetention.None -> {
                    _stores.remove(store.storeKey)
                    store.destroy()
                }
                is ComponentRetention.Temporary -> {
                    store.accessTimestamp = configuration.timeSource.currentTime()
                    configuration.scheduler.executeDelayed(
                        retention.retentionTime + STORE_EXPIRATION_DELAY,
                        this::destroyExpiredStores
                    )
                }
            }
        }
    }


    // Implementation private
    private fun requireChildren() = _children
        ?: mutableMapOf<Class<*>, Container<*>>().also { _children = it }

    private fun destroyExpiredStores(){
        if(_isDestroyed)
            return

        val time = configuration.timeSource.currentTime()
        val removedStores = mutableListOf<Store>()

        _stores.entries.removeAll {
            if(it.value.useCounter == 0 && it.value.accessTimestamp <= time){
                removedStores.add(it.value)
                true
            } else false
        }

        if(removedStores.isNotEmpty())
            removedStores.forEach { it.destroy() }

        if(requiresDestruction)
            destroy()
    }

    private val requiresDestruction: Boolean
        get() = _useCounter == 0                    // not acquired
                && _stores.isEmpty()                // no stores left
                && (_children?.isEmpty() ?: true)   // no children left
                && !_isDestroyed

    private fun destroy(){

        configuration.logger.logd(TAG, "DESTROYED. Component: ${componentClass.simpleName}, store: ${_stores.keys}, usage counter: $_useCounter.")

        assert(!_isDestroyed)
        _isDestroyed = true
        _sharedStore.destroy()
        parent?.removeChildContainer(this)
    }

}

internal fun <C> Container<*>.acquireOrCreateNewContainer(
    componentClass: Class<C>,
    buildComponent: ()->C
): ContainerHandle<C> {
    return getChildContainer(componentClass)
        ?: addChildContainer(componentClass, buildComponent())
}