package com.frumle.components

interface ComponentBuilder <D, C> {

    // Provide dependency to the Builder implementation.
    // returns the builder for the chaining of calls.
    fun dependency(dep: D): ComponentBuilder<D, C>

    // Build the component
    fun build(): C

}
