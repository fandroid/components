package com.frumle.components.android.utils

import com.frumle.components.utils.TimeSource

class AndroidTimeSource: TimeSource {

    override fun currentTime(): Long = System.currentTimeMillis()

}