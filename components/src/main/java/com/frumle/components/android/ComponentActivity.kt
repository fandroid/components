package com.frumle.components.android

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.frumle.components.ComponentOwner
import com.frumle.components.core.ComponentManager
import com.frumle.components.core.Container


abstract class ComponentActivity: AppCompatActivity(), ComponentOwner {

    internal class NonConfig(): ViewModel(){

        val componentManager = ComponentManager()

        private var _isCreated = false
        private var _isCleared = false

        fun onActivityCreate(config: ComponentManager.Configuration){
            check(!_isCleared)
            _isCreated = true
            componentManager.apply {
                this.configuration.set(config)
                acquireContainer()
            }
            Log.d("Components", "onCreate: container acquired: ${config.ownerClass!!.simpleName}, container: ${componentManager.container}")
        }

        fun onActivityDestroy(){
            _isCreated = false
            componentManager.container.clearContextualData()
            if(_isCleared)
                componentManager.releaseContainer()
        }

        override fun onCleared() {
            _isCleared = true
            if(!_isCreated)
                componentManager.releaseContainer()
        }

    }

    private val nonConfig by viewModels<NonConfig>()

    override val componentOwnerConfiguration = ComponentManager.Configuration()
    override val componentManager get() = nonConfig.componentManager
    override val container: Container<*> get() = componentManager.container

    override fun onCreate(savedInstanceState: Bundle?) {
        nonConfig.onActivityCreate(componentOwnerConfiguration)
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        nonConfig.onActivityDestroy()
        super.onDestroy()
    }

}