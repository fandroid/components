package com.frumle.components.android.utils

import android.os.Handler
import com.frumle.components.utils.Scheduler

class AndroidScheduler(private val handler: Handler): Scheduler {

    override fun executeDelayed(time: Long, task: Runnable) {
        handler.postDelayed(task, time)
    }

}