package com.frumle.components.android

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.frumle.components.core.ComponentManager
import com.frumle.components.ComponentNode
import com.frumle.components.ComponentOwner
import com.frumle.components.core.Container

open class ComponentFragment: Fragment, ComponentOwner {

    constructor() : super()
    constructor(@LayoutRes layoutResId: Int): super(layoutResId)

    final override val parentNode: ComponentNode?
        get() {
            var parentFragment: Fragment? = parentFragment
            while (parentFragment != null){
                if(parentFragment is ComponentNode)
                    return parentFragment
                parentFragment = parentFragment.parentFragment
            }

            val parentActivity = activity
            if(parentActivity is ComponentNode)
                return parentActivity

            return null
        }

    final override val container: Container<*> get() = componentManager.container

    override val componentOwnerConfiguration = ComponentManager.Configuration()
    final override val componentManager = ComponentManager()

    override fun onCreate(savedInstanceState: Bundle?) {
        componentManager.apply {
            configuration.set(componentOwnerConfiguration)
            acquireContainer()
        }
        super.onCreate(savedInstanceState)
    }

    override fun onDestroy() {
        componentManager.releaseContainer()
        super.onDestroy()
    }

}