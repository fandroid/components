package com.frumle.components.android.utils

import android.util.Log
import com.frumle.components.utils.Logger

class AndroidLogcatLogger(private val tag: String): Logger {

    override fun logd(tag: String, msg: String) {
        Log.d(this.tag, "$tag: $msg")
    }

    override fun logw(tag: String, msg: String) {
        Log.w(this.tag, "$tag: $msg")
    }

    override fun loge(tag: String, msg: String) {
        Log.e(this.tag, "$tag: $msg")
    }

}