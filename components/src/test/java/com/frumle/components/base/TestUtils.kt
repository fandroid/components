package com.frumle.components.base

import com.frumle.components.core.ComponentsConfiguration
import com.frumle.components.doubles.TestScheduler
import com.frumle.components.doubles.TestTimeSource

class TestConfiguration {
    val timeSource: TestTimeSource = TestTimeSource(0)
    val scheduler: TestScheduler = TestScheduler(timeSource)
}

fun TestConfiguration.toContainerConfiguration(): ComponentsConfiguration {
    return ComponentsConfiguration(timeSource, scheduler)
}

//fun <C> makeContainer(component: C): Container<C> {
//
//}