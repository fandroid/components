package com.frumle.components.doubles

import com.frumle.components.utils.TimeSource


class TestTimeSource(
    private var time: Long = 0L
): TimeSource {

    fun proceed(by: Long){ time += by }

    override fun currentTime() = time
}