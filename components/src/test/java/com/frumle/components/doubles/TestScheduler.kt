package com.frumle.components.doubles

import com.frumle.components.utils.Scheduler
import com.frumle.components.utils.TimeSource

class TestScheduler(
    private val timeSource: TimeSource
): Scheduler {

    private class Task(val time: Long, val runnable: Runnable)

    private val _tasks = mutableListOf<Task>()

    override fun executeDelayed(time: Long, task: Runnable) {
        _tasks += Task(time, task)
    }

    fun flush(){
        val time = timeSource.currentTime()
        _tasks.removeIf {
            if(it.time <= time){
                it.runnable.run()
                true
            } else false
        }
    }

    val tasksCount = _tasks.size

}